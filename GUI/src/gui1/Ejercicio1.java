

package gui1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;

public class Ejercicio1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio1 frame = new Ejercicio1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivos = new JMenu("Archivos");
		menuBar.add(mnArchivos);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivos.add(mntmAbrir);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivos.add(mntmGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como");
		mnArchivos.add(mntmGuardarComo);
		
		JMenu mnEdicion = new JMenu("Edicion");
		menuBar.add(mnEdicion);
		
		JMenuItem mntmFuente = new JMenuItem("Fuente");
		mnEdicion.add(mntmFuente);
		
		JMenuItem mntmColor = new JMenuItem("Color");
		mnEdicion.add(mntmColor);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmTutoriales = new JMenuItem("Tutoriales");
		mnAyuda.add(mntmTutoriales);
		
		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mnAyuda.add(mntmBuscar);
		
		JMenuItem mntmComandos = new JMenuItem("Comandos");
		mnAyuda.add(mntmComandos);
		
		JMenu mnOpciones = new JMenu("Opciones");
		menuBar.add(mnOpciones);
		
		JMenuItem mntmTamaoDelMen = new JMenuItem("Tama\u00F1o del men\u00FA");
		mnOpciones.add(mntmTamaoDelMen);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1549926000000L), new Date(1546297200000L), new Date(4099762800000L), Calendar.DAY_OF_YEAR));
		spinner.setBounds(42, 190, 89, 20);
		contentPane.add(spinner);
		
		JRadioButton rdbtnMayusculas = new JRadioButton("Mayusculas");
		rdbtnMayusculas.setBounds(28, 23, 109, 23);
		contentPane.add(rdbtnMayusculas);
		
		JCheckBox chckbxSubrayar = new JCheckBox("Subrayar");
		chckbxSubrayar.setBounds(28, 49, 97, 23);
		contentPane.add(chckbxSubrayar);
		
		JCheckBox chckbxNegrita = new JCheckBox("Negrita");
		chckbxNegrita.setBounds(28, 75, 97, 23);
		contentPane.add(chckbxNegrita);
		
		JCheckBox chckbxCursiva = new JCheckBox("Cursiva");
		chckbxCursiva.setBounds(28, 102, 97, 23);
		contentPane.add(chckbxCursiva);
		
		JButton btnBorrarTodo = new JButton("Borrar texto");
		btnBorrarTodo.setBounds(174, 189, 109, 23);
		contentPane.add(btnBorrarTodo);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Guardar", "Guardar como", "Abrir", "Borrar archivo"}));
		comboBox.setBounds(303, 190, 97, 20);
		contentPane.add(comboBox);
		
		JLabel lblTamao = new JLabel("Fecha");
		lblTamao.setBounds(6, 192, 58, 14);
		contentPane.add(lblTamao);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(400, 159, -244, -134);
		contentPane.add(scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(400, 159, -242, -134);
		contentPane.add(scrollPane_1);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(379, 22, -215, 116);
		contentPane.add(textArea);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(174, 23, 226, 143);
		contentPane.add(scrollPane_2);
		
		JTextArea textArea_1 = new JTextArea();
		scrollPane_2.setViewportView(textArea_1);
	}
}
