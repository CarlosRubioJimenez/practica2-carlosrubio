package main;

import java.util.Scanner;

import libreria.Libreria;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		int opcion;
		boolean menu = true;
		
		while(menu == true){
			System.out.println("\t\n*** MEN� ***");
			System.out.println("1. Calcular si un n�mero es par o impar.");
			System.out.println("2. Calcular si un n�mero es primo.");
			System.out.println("3. Calcular la media de un conjunto de n�meros.");
			System.out.println("4. Contar cu�ntas veces se repite un valor en un conjunto de n�meros.");
			System.out.println("5. Salir.");
			System.out.print("Opci�n: ");
			opcion=in.nextInt();
			while(opcion<1 || opcion>5){
				System.out.println("Error. Introduce un valor entre 1 y 5.");
				System.out.print("Opci�n: ");
				opcion=in.nextInt();
			}
				
			if(opcion == 1){
				System.out.print("Escribe el n�mero que quieres comprobar: ");
				int num = in.nextInt();
				boolean par = Libreria.parImpar(num);
				if(par == true){
					System.out.println(num + " es par.");
				}
				else System.out.println(num + " no es par.");
			}
			else if(opcion == 2){
				System.out.print("Escribe el n�mero que quieres comprobar: ");
				int num = in.nextInt();
				boolean primo = Libreria.esPrimo(num);
				if(primo == true){ 
					System.out.println(num + " es primo.");
				}
				else if(primo == false){ 
					System.out.println(num + " no es primo.");
				}
					
			}else if(opcion == 3){
				System.out.print("Escribe cuantos n�meros vas a introducir: ");
				int dimension = in.nextInt();
				int[] v = new int [dimension];
				float media = Libreria.calcularMedia(v);
				System.out.println("La media es " + media);
			}
			else if(opcion == 4){
				System.out.print("Escribe cuantos n�meros vas a introducir: ");
				int dimension = in.nextInt();
				int[] v = new int [dimension];
				System.out.print("Escribe el valor que quieres ver cuantas veces se repite: ");
				int valorABuscar=in.nextInt();
				int contador = Libreria.vecesRepetido(v, valorABuscar);
				System.out.println("El " + valorABuscar + " se repite " + contador + " veces.");
			}
			else if(opcion == 5){
				System.out.println("***FIN***");
				menu = false;
			}
			
		}
	}
}


